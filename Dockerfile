FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/data
WORKDIR /app/code

# when external repo is added, apt-get will install the latest in case of conflicting name. apt-cache policy <name> will show what is getting used
# so the remove of 7.4 is probably superfluous but here for completeness
RUN apt-get remove -y php-* php7.4-* libapache2-mod-php7.4 && \
    apt-get autoremove -y && \
    add-apt-repository --yes ppa:ondrej/php && \
    apt update && \
    apt install -y php8.1 php8.1-{apcu,bcmath,bz2,cgi,cli,common,curl,dba,dev,enchant,fpm,gd,gmp,gnupg,imagick,imap,interbase,intl,ldap,mailparse,mbstring,mysql,odbc,opcache,pgsql,phpdbg,pspell,readline,redis,snmp,soap,sqlite3,sybase,tidy,uuid,xml,xmlrpc,xsl,zip,zmq} libapache2-mod-php8.1 && \
    apt install -y php-{date,pear,twig,validate} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# this binaries are not updated with PHP_VERSION since it's a lot of work
RUN update-alternatives --set php /usr/bin/php8.1 && \
    update-alternatives --set phar /usr/bin/phar8.1 && \
    update-alternatives --set phar.phar /usr/bin/phar.phar8.1 && \
    update-alternatives --set phpize /usr/bin/phpize8.1 && \
    update-alternatives --set php-config /usr/bin/php-config8.1

# configure apache
# keep the prefork linking below a2enmod since it removes dangling mods-enabled (!)
# perl kills setlocale() in php - https://bugs.mageia.org/show_bug.cgi?id=25411
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 80" > /etc/apache2/ports.conf && \
    a2enmod rewrite headers rewrite expires cache ldap authnz_ldap && \
    a2dismod perl && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf && \
    ln -sf /app/data/apache/app.conf /etc/apache2/sites-enabled/app.conf

COPY apache/ /app/code/apache/

# configure mod_php
RUN \
        crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
        crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
        crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 128M && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable 1 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable_cli 1 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.memory_consumption 128 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.save_comments 1 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.validate_timestamps 1 && \
        crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.revalidate_freq 60 && \
        crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/app/sessions && \
        crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
        crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

# shlink
ARG VERSION=3.5.2

RUN wget https://github.com/shlinkio/shlink/releases/download/v${VERSION}/shlink${VERSION}_php8.1_dist.zip
RUN unzip shlink${VERSION}_php8.1_dist.zip && mv shlink${VERSION}_php8.1_dist/* /app/code/ && \
    rm -rf /app/code/shlink${VERSION}_php8.1_dist/ && rm /app/code/shlink${VERSION}_php8.1_dist.zip
RUN mv /app/code/data /app/code/data_orig && \
    ln -sf /app/data/data /app/code/data

# add code
COPY start.sh /app/code/

RUN chown -R www-data.www-data /app/code /app/data

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

CMD [ "/app/code/start.sh" ]
