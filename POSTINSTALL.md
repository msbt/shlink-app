After installation, hop on a terminal and run `/app/code/bin/cli api-key:generate` to create your API key

Use https://app.shlink.io/ (or put the [https://github.com/shlinkio/shlink-web-client](web client) on surfer to add your short URLs (add your domain without trailing slash, like `https://shlink.example.com`)

After logging in, go to `Manage Domains` and set/edit the redirect domains.
