#!/bin/bash

set -eu

mkdir -p /app/data/data /run/apache2 /run/app/sessions /app/data/apache

# copy files 
if [[ ! -f /app/data/initialized ]]; then
    echo "==> Generate files on first run"
    touch /app/data/initialized
    
    echo "==> Copy Data Directory"
    cp -r /app/code/data_orig/* /app/data/data

    echo -e "#!/bin/bash\n\n# Place custom startup commands here" > /app/data/run.sh
  
cat <<\EOT >> /app/data/run.sh
# general
export DEFAULT_DOMAIN="${CLOUDRON_APP_DOMAIN}"
export IS_HTTPS_ENABLED=true
export GEOLITE_LICENSE_KEY=
export AUTO_RESOLVE_TITLES=false
export DEFAULT_SHORT_CODES_LENGTH=5
#export DELETE_SHORT_URL_THRESHOLD=
#export TIMEZONE="Europe/Vienna"
export MULTI_SEGMENT_SLUGS_ENABLED=false
export SHORT_URL_TRAILING_SLASH=false
export SHORT_URL_MODE=strict

# database
export DB_DRIVER=mysql
export DB_NAME="${CLOUDRON_MYSQL_DATABASE}"
export DB_USER="${CLOUDRON_MYSQL_USERNAME}"
export DB_PASSWORD="${CLOUDRON_MYSQL_PASSWORD}"
export DB_HOST="${CLOUDRON_MYSQL_HOST}"
export DB_PORT="${CLOUDRON_MYSQL_PORT}"

# redirects
export REDIRECT_STATUS_CODE=302
export REDIRECT_CACHE_LIFETIME=30
export DEFAULT_INVALID_SHORT_URL_REDIRECT=
export DEFAULT_REGULAR_404_REDIRECT=
export DEFAULT_BASE_URL_REDIRECT=
export REDIRECT_APPEND_EXTRA_PATH=false

# tracking
export DISABLE_TRACKING=false
export TRACK_ORPHAN_VISITS=true
#export DISABLE_TRACK_PARAM=
export DISABLE_TRACKING_FROM=172.18.*.*
export DISABLE_IP_TRACKING=false
export DISABLE_REFERRER_TRACKING=false
export DISABLE_UA_TRACKING=false
export ANONYMIZE_REMOTE_ADDR=true

# qr codes
export DEFAULT_QR_CODE_SIZE=300
export DEFAULT_QR_CODE_MARGIN=0
export DEFAULT_QR_CODE_FORMAT=png
export DEFAULT_QR_CODE_ERROR_CORRECTION=l
export DEFAULT_QR_CODE_ROUND_BLOCK_SIZE=true

# mercure
export MERCURE_PUBLIC_HUB_URL=
export MERCURE_INTERNAL_HUB_URL=
export MERCURE_JWT_SECRET=

# redis
#export REDIS_SERVERS=
#export REDIS_SENTINEL_SERVICE=
#export REDIS_PUB_SUB_ENABLED=false
EOT

    source /app/data/run.sh
  
    echo "==> Create Tables"
    /app/code/bin/cli db:create
    
    echo "==> Create Migrations"
    /app/code/bin/cli db:migrate

    echo "==> Create Proxies"
    php /app/code/vendor/doctrine/orm/bin/doctrine.php orm:generate-proxies

    echo "==> Apache stuff"
    [[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/code/apache/mpm_prefork.conf /app/data/apache/mpm_prefork.conf
    [[ ! -f /app/data/apache/app.conf ]] && cp /app/code/apache/app.conf /app/data/apache/app.conf
fi

echo "==> Source custom startup script"
[[ -f /app/data/run.sh ]] && source /app/data/run.sh

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

echo "==> Starting Shlink"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

